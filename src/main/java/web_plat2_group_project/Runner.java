package web_plat2_group_project;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import web_plat2_group_project.db.H2Tracker;
import web_plat2_group_project.servlet.*;

public class Runner {

    private static final Logger LOG = LoggerFactory.getLogger(Runner.class);
    private static final int PORT = 9009;

    private final String name;
    private final H2Tracker h2Tracker;

    public Runner(String name) {
        this.name = name;
        h2Tracker = new H2Tracker();
    }

    private void start() throws Exception {
        Server server = new Server(PORT);

        ServletContextHandler handler = new ServletContextHandler(server, "/", ServletContextHandler.SESSIONS);
        handler.setContextPath("/");
        handler.setInitParameter("org.eclipse.jetty.servlet.Default." + "resourceBase", "src/main/resources/webapp");

        DefaultServlet ds = new DefaultServlet();
        handler.addServlet(new ServletHolder(ds), "/");

        /*MilestoneServlet milestoneServlet = new MilestoneServlet(h2Tracker);
        handler.addServlet(new ServletHolder(milestoneServlet), "/milestone/*");*/

        /*TrackerServlet trackerServlet = new TrackerServlet();
        handler.addServlet(new ServletHolder(trackerServlet), "/tracker");*/

        handler.addServlet(new ServletHolder(new MilestoneServlet(h2Tracker)), "/milestone/*");
        handler.addServlet(new ServletHolder(new TrackerServlet(h2Tracker)), "/tracker"); // we post to here
        handler.addServlet(new ServletHolder(new LoginServlet(h2Tracker)), "/login");
        handler.addServlet(new ServletHolder(new RegisterServlet(h2Tracker)), "/register");
        handler.addServlet(new ServletHolder(new LogoutServlet()), "/logout");

        server.start();
        LOG.info("Server started, will run until terminated");
        server.join();

    }

    public static void main(String[] args) {
        try {
            LOG.info("server starting...");
            new Runner("Demo").start();
        } catch (Exception e) {
            LOG.error("Unexpected error running: " + e.getMessage());
        }
    }

}
