package web_plat2_group_project.db;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import web_plat2_group_project.model.Milestone;
import web_plat2_group_project.model.Tracker;
import web_plat2_group_project.model.User;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class H2Tracker  implements AutoCloseable {
    @SuppressWarnings("unused")
    static final Logger LOG = LoggerFactory.getLogger(H2Tracker.class);

    public static final String FILE = "jdbc:h2:~/tracker";

    private Connection connection;

    static Connection getConnection(String db) throws SQLException, ClassNotFoundException {
        Class.forName("org.h2.Driver");  // ensure the driver class is loaded when the DriverManager looks for an installed class. Idiom.
        return DriverManager.getConnection(db, "trackeruser", "");  // default password, ok for embedded.
    }

    public H2Tracker() {
        this(FILE);
    }

    public H2Tracker(String db) {
        try {
            connection = getConnection(db);
            loadResource("/tracker.sql");
        } catch (ClassNotFoundException | SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void close() {
        try {
            if (connection != null) {
                connection.close();
                connection = null;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void addMilestone(Milestone milestone) {
        final String ADD_MILESTONE_QUERY = "INSERT INTO tracker (description, duedate, user) VALUES (?,?,?)";
        try (PreparedStatement ps = connection.prepareStatement(ADD_MILESTONE_QUERY)) {
            ps.setString(1, milestone.getDescription());
            ps.setString(2, milestone.getDueDate());
            ps.setString(3,milestone.getUser());
            ps.execute();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void updateMilestone(Milestone milestone) {
        int id = milestone.getId();
        String desc = milestone.getDescription();
        String due = milestone.getDueDate();
        String comp = milestone.getCompletedDate();

        String UPDATE_MILESTONE_QUERY = "update tracker set description = ?, duedate = ?, completeddate = ? where id = ?";
        try(PreparedStatement ps = connection.prepareStatement(UPDATE_MILESTONE_QUERY))
        {
            ps.setString(1,desc);
            ps.setString(2,due);
            ps.setString(3,comp);
            ps.setInt(4,id);
            ps.execute();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void addUser(User user) {
        System.out.println(user);
        final String ADD_USER_QUERY = "INSERT INTO users (username, password) VALUES (?,?)";
        try (PreparedStatement ps = connection.prepareStatement(ADD_USER_QUERY)) {
            ps.setString(1, user.getUsername());
            ps.setString(2, user.getPassword());
            ps.execute();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public List<User> getAllUsers(){
        final String GET_USERS_QUERY = "SELECT username, password  FROM users";
        List<User> out = new ArrayList<>();
        try (PreparedStatement ps = connection.prepareStatement(GET_USERS_QUERY)) {
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                out.add(new User(rs.getString(1), rs.getString(2)));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return out;
    }

    public List<Milestone> findMilestones() {
        final String LIST_MILESTONES_QUERY = "SELECT id, description, duedate, completeddate, user  FROM tracker";
        List<Milestone> out = new ArrayList<>();
        try (PreparedStatement ps = connection.prepareStatement(LIST_MILESTONES_QUERY)) {
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                out.add(new Milestone(rs.getInt(1), rs.getString(5), rs.getString(2), rs.getString(3), rs.getString(4)));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return out;
    }

    public List<Milestone> findUserMilestones(String user) {
        final String LIST_MILESTONES_QUERY = "SELECT id, description, duedate, completeddate, user  FROM tracker";
        List<Milestone> out = new ArrayList<>();
        try (PreparedStatement ps = connection.prepareStatement(LIST_MILESTONES_QUERY)) {
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                if(rs.getString(5).equals(user)) {
                    out.add(new Milestone(rs.getInt(1), rs.getString(5), rs.getString(2), rs.getString(3), rs.getString(4)));
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return out;
    }


    public Milestone getMilestone(int id) {
        List<Milestone> milestones= findMilestones();
        Milestone newMs = new Milestone();
        for(Milestone ms: milestones)
        {
            if(ms.getId() == id)
            {
                return ms;
            }
        }
        return newMs;
    }

    public void deleteMilestone(int id) {
        String DELETE_MILESTONE_QUERY = "DELETE FROM tracker WHERE id = ?";

        try (PreparedStatement ps = connection.prepareStatement(DELETE_MILESTONE_QUERY)) {
            ps.setInt(1, id);
            ps.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    private void loadResource(String name) {
        try {
            String cmd = new Scanner(getClass().getResource(name).openStream()).useDelimiter("\\Z").next();
            PreparedStatement ps = connection.prepareStatement(cmd);
            ps.execute();
        } catch (SQLException | IOException e) {
            throw new RuntimeException(e);
        }
    }
}
