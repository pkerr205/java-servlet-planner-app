package web_plat2_group_project.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Milestone {

    private int id;
    private String user;
    private String description;
    private String dueDate;
    private String completedDate;


    public Milestone(int id, String desc, String due)
    {
        this.id = id;
        this.description = desc;
        this.dueDate = due;
    }

    public Milestone(String user, String desc, String due)
    {
        this.user = user;
        this.description = desc;
        this.dueDate = due;
    }

}
