package web_plat2_group_project.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;


@AllArgsConstructor
@Data
public class Tracker {

    private String name;
    private List<Milestone> milestones;

    public Tracker(String name)
    {
        this.name = name;
        this.milestones = new ArrayList<Milestone>();
    }

    public void addMilestone(Milestone ms) { milestones.add(ms); }

}
