package web_plat2_group_project.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class User
{

    private String username;
    private String password;

}
