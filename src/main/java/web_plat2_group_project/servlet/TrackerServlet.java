package web_plat2_group_project.servlet;

import lombok.Data;
import web_plat2_group_project.db.H2Tracker;
import web_plat2_group_project.model.Milestone;
import web_plat2_group_project.model.Tracker;
import web_plat2_group_project.util.MustacheRenderer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.Charset;
import java.sql.Date;
import java.util.List;

import static java.sql.Date.valueOf;

public class TrackerServlet extends BaseServlet {


    //good practice to declare the template that is populated as a constant, why?
    //declare your template here
    private static final String MESSAGE_BOARD_TEMPLATE = "tracker.mustache";
    //servlet can be serialized
    private static final long serialVersionUID = 687117339002032958L;
    Tracker tracker;

    private final H2Tracker h2Tracker;
    private final MustacheRenderer mustache;

    public TrackerServlet(H2Tracker h2Tracker)  {
        mustache = new MustacheRenderer();
        tracker = new Tracker("Milestone Tracker");
        this.h2Tracker = h2Tracker;
    }

    //right now, setting the data for the page by hand, later the data will come from a data store
    private Object getObject() {
        return tracker;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (!authOK(request, response)) {
            return;
        }
        String user = UserFuncs.getCurrentUser(request);
        List<Milestone> milestones = h2Tracker.findUserMilestones(user);
        String html = mustache.render("tracker.mustache", new Result(tracker.getName(), milestones));
        response.setContentType("text/html");
        response.setStatus(200);
        response.getOutputStream().write(html.getBytes(Charset.forName("utf-8")));
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String user = UserFuncs.getCurrentUser(request);
        String desc = request.getParameter("desc");
        String due = request.getParameter("due");
        Milestone ms = new Milestone(user, desc, due);

        System.out.println(ms);
        h2Tracker.addMilestone(ms);
        response.sendRedirect("/tracker");
    }

    @Data
    class Result {
        String name;
        List<Milestone> milestones;
        Result( String name, List<Milestone> milestones)
        {
            this.name = name;
            this.milestones = milestones;
        }
    }

}