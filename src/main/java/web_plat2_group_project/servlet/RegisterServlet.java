// Copyright (c) 2018 Cilogi. All Rights Reserved.
//
// File:        LoginServlet.java
//
// Copyright in the whole and every part of this source file belongs to
// Cilogi (the Author) and may not be used, sold, licenced, 
// transferred, copied or reproduced in whole or in part in 
// any manner or form or in or on any media to any person other than 
// in accordance with the terms of The Author's agreement
// or otherwise without the prior written consent of The Author.  All
// information contained in this source file is confidential information
// belonging to The Author and as such may not be disclosed other
// than in accordance with the terms of The Author's agreement, or
// otherwise, without the prior written consent of The Author.  As
// confidential information this source file must be kept fully and
// effectively secure at all times.
//


package web_plat2_group_project.servlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import web_plat2_group_project.db.H2Tracker;
import web_plat2_group_project.model.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


public class RegisterServlet extends BaseServlet {
    @SuppressWarnings("unused")
    static final Logger LOG = LoggerFactory.getLogger(RegisterServlet.class);

    private final String LOGIN_TEMPLATE = "login.mustache";
    private final H2Tracker h2Tracker;


    public RegisterServlet(H2Tracker h2Tracker) {
        this.h2Tracker = h2Tracker;
    }


    public boolean checkUsername(String userName) {
        List<User> users = h2Tracker.getAllUsers();
        for (User user: users) {
            System.out.println(user + " checked");
            if(user.getUsername().equals(userName))
            {
                return true;
            }
        }
        return false;
    }

    public void addNewUser(String userName, String password)
    {
            User newUser = new User(userName, password);
            h2Tracker.addUser(newUser);
            System.out.println(newUser + " added");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String userName = UserFuncs.getCurrentUser(request);
        showView(response, LOGIN_TEMPLATE, userName);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        String name = request.getParameter(UserFuncs.USERNAME_REG_PARAMETER);
        String password = request.getParameter(UserFuncs.PASSWORD_REG_PARAMETER);
        String cpassword = request.getParameter(UserFuncs.CPASSWORD_REG_PARAMETER);
        System.out.println("name: " + name + ", password: " + password + ", confirm password: " + cpassword);
        if (name != null && name.length() > 0 && password != null && password.length() > 0 && cpassword != null && cpassword.length() > 0)
        {
            if (password.equals(cpassword))
            {
                if(checkUsername(name) != true)
                {
                    addNewUser(name, password);
                    UserFuncs.setCurrentUser(request, name);
                    String targetURL = UserFuncs.getLoginRedirect(request);
                    response.sendRedirect(response.encodeRedirectURL(targetURL));
                    System.out.println(name + " logged in");
                }
                else
                {
                    String warning = "username already exists";
                    showView(response, LOGIN_TEMPLATE, warning);
                }
            }
            else
            {
                String warning = "passwords don't match";
                showView(response, LOGIN_TEMPLATE, warning);
            }
        }
        else
        {
            String warning = "please fill in all details";
            showView(response, LOGIN_TEMPLATE, warning);
        }
        // do nothing, we stay on the page,
        // could also display a warning message by passing parameter to /login on redirect
    }
}
