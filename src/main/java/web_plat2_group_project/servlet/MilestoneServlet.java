package web_plat2_group_project.servlet;

import web_plat2_group_project.db.H2Tracker;
import web_plat2_group_project.model.Milestone;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;

import static java.sql.Date.valueOf;

public class MilestoneServlet extends BaseServlet {

    private static final String TOPIC_TEMPLATE = "milestone.mustache";
    private final H2Tracker h2Tracker;

    public MilestoneServlet(H2Tracker h2Tracker) {
        this.h2Tracker = h2Tracker;
    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String path = request.getRequestURI();
        String idStr = path.substring(path.lastIndexOf('/') + 1);
        int id = Integer.parseInt(idStr);
        String[] uriSections = request.getRequestURI().split("/");
        System.out.println(uriSections[2]);
        if(uriSections[2].equals("edit")) {
            Milestone ms = new Milestone();
            ms = h2Tracker.getMilestone(id);
            showView(response, TOPIC_TEMPLATE, ms);
        }
        else if(uriSections[2].equals("delete"))
        {
            h2Tracker.deleteMilestone(id);
            response.sendRedirect("/tracker");
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = Integer.valueOf(request.getParameter("id"));
        String user = UserFuncs.getCurrentUser(request);
        String desc = request.getParameter("description");
        String due = request.getParameter("dueDate");
        String comp = request.getParameter("completedDate");
        Milestone ms = new Milestone(id, user, desc, due, comp);

        System.out.println(ms);
        h2Tracker.updateMilestone(ms);
        response.sendRedirect("/tracker");
    }

}

