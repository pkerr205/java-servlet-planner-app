CREATE TABLE IF NOT EXISTS tracker (
  id int AUTO_INCREMENT PRIMARY KEY,
  description VARCHAR(255),
  duedate VARCHAR(255),
  completeddate VARCHAR(255),
  user VARCHAR(255)
);

CREATE TABLE IF NOT EXISTS users (
  id int AUTO_INCREMENT PRIMARY KEY,
  username VARCHAR(255),
  password VARCHAR(255),
);

